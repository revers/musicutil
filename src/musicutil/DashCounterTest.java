package musicutil;

import java.util.*;
import java.util.logging.LogManager;
import java.io.*;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;

public class DashCounterTest {

	private ArrayList<File> getFilesRecursive(File dir, FileFilter filter,
			ArrayList<File> mp3List) {
		if (mp3List == null) {
			mp3List = new ArrayList<File>();
		}
		File[] files = dir.listFiles();

		for (File f : files) {
			if (f.isDirectory()) {
				getFilesRecursive(f, filter, mp3List);
			}
			if (filter.accept(f)) {
				mp3List.add(f);
			}
		}

		return mp3List;
	}

	private File[] getFiles(File dir, FileFilter filter, boolean recursive) {
		ArrayList<File> mp3List = null;
		if (recursive) {
			mp3List = getFilesRecursive(dir, filter, null);

		} else {
			mp3List = new ArrayList<File>();

			File[] files = dir.listFiles();
			for (File f : files) {
				if (filter.accept(f)) {
					mp3List.add(f);
				}
			}
		}

		File[] result = mp3List.toArray(new File[] {});
		return result;
	}

	private int countDashes(String name) {
		int dashes = 0;

		for (int i = 0; i < name.length(); i++) {
			if (name.charAt(i) == '-')
				dashes++;
		}
		return dashes;
	}

	private void test() {
		File dir = new File(// "D:\\Muzyka");
				"D:\\[MUZYKA]\\");
		// "D:\\[MUZYKA]\\[[[z Downloads]]]");
		FileFilter filter = new FileFilter() {

			@Override
			public boolean accept(File f) {
				if (f.isDirectory())
					return false;
				if (f.getName().toLowerCase().endsWith(".mp3")) {
					return true;
				}
				return false;
			}
		};
		File[] mp3Files = getFiles(dir, filter, true);

		int allDashed = 0;
		int titleDashed = 0;
		int artistDashed = 0;

		for (File f : mp3Files) {

			String name = f.getName();
			int dashes = countDashes(name);

			if (dashes > 1) {

				try {
					AudioFile audioFile = AudioFileIO.read(f);
					Tag tag = audioFile.getTag();

					String artist = tag.getFirst(FieldKey.ARTIST);
					String title = tag.getFirst(FieldKey.TITLE);

					int artistDashes = countDashes(artist);
					int titleDashes = countDashes(title);

					if (artistDashes > titleDashes) {
						artistDashed++;
						allDashed++;
						//System.out.println(f.getName());
						//System.out.println("\tARTIST: " + artist);
						//System.out.println("\tTITLE: " + title + "\n");
					} else if (artistDashes < titleDashes) {
						titleDashed++;
						allDashed++;
						System.out.println(f.getName());
						System.out.println("\tARTIST: " + artist);
						System.out.println("\tTITLE: " + title + "\n");
					}

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}
		}

		System.out.println("WSZYSTKICH Z WIECEJ NIZ JEDNYM MYSLNIKIEM: "
				+ allDashed);
		System.out.println("ARTYSTA WIECEJ: " + artistDashed);
		System.out.println("TYTUL WIECEJ: " + titleDashed);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LogManager.getLogManager().reset();
		DashCounterTest dct = new DashCounterTest();
		dct.test();
	}

}
