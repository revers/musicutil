package musicutil;

import org.apache.commons.lang.StringEscapeUtils;
import org.jaudiotagger.audio.*;
import org.jaudiotagger.audio.exceptions.*;
import org.jaudiotagger.tag.*;
import org.jaudiotagger.tag.id3.*;
import org.jaudiotagger.tag.id3.framebody.*;

import org.cmc.music.common.*;
import org.cmc.music.myid3.*;
import org.cmc.music.metadata.*;

import java.util.*;
import java.net.*;
import java.io.*;
import java.util.regex.*;
import musicutil.mp3.MyTag;
import musicutil.mp3.TagFinder;
import org.jaudiotagger.tag.datatype.Artwork;

/**
 *
 * @author Revers
 */
public class MusicUtil {

    public static final String[] STANDARD_BANNED_PREFIXES = {"[",
        "- upload for 4cl",
        "- upload for www",
        "- upload for http",
        "-upload for www",
        "-upload for 4cl",
        "-upload http",
        "upload for 4cl",
        "upload for www",
        "upload for http",
        "- up for http",
        "- up for www",
        "- up for 4cl",
        "-up for 4cl",
        "-up for www",
        "-up for http",
        "up for www",
        "up for 4cl",
        "up for http",
        "(by djohnny)",
        "(www.",
        "(http",
        "- www.",
        "-www.",
        "- http:",
        "-http:",
        "http:",
        " - www.4clubb",
        "- 4clubb",
        "-4clubb",
        "-www.4clubb", "(vclub",
        "(4clubb",
        "(4clabb",
        "www.4clubb",
        "4clubb",
        " - www.4clabb",
        "- 4clabb",
        "-4clabb",
        "-www.4clabb",
        "www.4clabb",
        "4clabb",
        " - www.vclub",
        "- vclub",
        "-vclub",
        "-www.vclub",
        "www.vclub",
        "vclub",
        "(by Miko34)",
        "pobrano z",
        "www.",
        " ivan - fresh-"};
    public static final String[] STANDARD_BANNED_WORDS = {"\u6d00", "\u00b0"
    };
    public static final char[] PROHIBITED_CHARS = {'\\', '/', ':', '*', '?',
        '<', '>', '|'};
    public static final String[] REMIX_ENDS = {"rerub", "rework", "re-work", "re-fix",
        "edit", "mix", "remix", "refix", "rmx", "remixed", "re-edit", "dub", "bootleg",
        "redub", "re-rub", "extended", "remake"};
    private static final String[] EXCEPTIONS = {"vs", "vs.", "feat", "feat.",
        "and", "or", "ft", "ft."};
    private String[] bannedPrefixes;
    private String[] bannedWords;

    public MusicUtil() {
        this.bannedPrefixes = STANDARD_BANNED_PREFIXES;
        this.bannedWords = STANDARD_BANNED_WORDS;
    }

    public MusicUtil(String[] bannedPrefixes, String[] bannedWords) {
        this.bannedPrefixes = bannedPrefixes;
        this.bannedWords = bannedWords;
    }

    private static String capitalizeSpecial(String s) {
        Pattern ptr = Pattern.compile("([\\(\\[\\.\\-,_]\\S)");
        Matcher mchr = ptr.matcher(s);
        while (mchr.find()) {
            String word = s.substring(mchr.start());

            String[] parts = word.split("\\s");
            word = parts[0].substring(1);

            boolean canChange = true;

            for (String exc : EXCEPTIONS) {
                if (word.equalsIgnoreCase(exc)) {
                    canChange = false;
                    break;
                }
            }

            if (canChange) {
                s = s.substring(0, mchr.start()) + mchr.group().toUpperCase()
                        + s.substring(mchr.end());
            } else {
                s = s.substring(0, mchr.start()) + parts[0].toLowerCase() + " ";
                for (int i = 1; i < parts.length; i++) {
                    s += parts[i] + " ";
                }
                s = s.trim();
            }
        }
        return s;
    }

    public static String capitalizeFirstLetters(String s) {

        String[] parts = s.split("\\s+");
        String result = "";
        for (String part : parts) {
            if (part.length() > 0) {

                boolean exception = false;
                for (String exc : EXCEPTIONS) {
                    if (part.equalsIgnoreCase(exc)) {
                        exception = true;
                        break;
                    }
                }

                if (exception) {
                    result += part + " ";
                } else {
                    result += part.substring(0, 1).toUpperCase();
                    result += part.substring(1) + " ";
                }

            }

        }
        result = result.trim();
        result = capitalizeSpecial(result);

        result = result.replaceAll("\\s{2,}", " ");
        result = result.trim();

        return result;
    }

    public static class Pair {

        public String artist;
        public String title;
    }

    private boolean isNumber(String s) {
        try {
            Integer.parseInt(s.trim());

            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private Pair extractTitleAndArtist(String cleanName) {
        String artist = cleanName;
        String title = "";

        String dash = " - ";
        int index = artist.indexOf(dash);
        if (index < 0) {
            dash = "-";
            index = artist.indexOf(dash);
        }

        if (index >= 0) {
            artist = cleanName.substring(0, index);
            title = cleanName.substring(index + dash.length(), cleanName.length());
        }

        if (isNumber(artist) && title.contains("-")) {
            cleanName = title;

            dash = " - ";
            index = cleanName.indexOf(dash);
            if (index < 0) {
                dash = "-";
                index = cleanName.indexOf(dash);
            }

            artist = cleanName.substring(0, index);
            title = cleanName.substring(index + dash.length(), cleanName.length());
        }

        Pair p = new Pair();
        p.artist = artist;
        p.title = title;

        return p;
    }

    public Pair getTitleAndArtistFromName(String name) {

        name = generalFixer(name);


        Pair p = extractTitleAndArtist(name);


        p.artist = capitalizeFirstLetters(p.artist);
        p.title = capitalizeFirstLetters(p.title);
        p.title = titleFixer(p.title);
        // System.out.printf("artist: %s%ntitle: %s%nOriginal: %S%n%n", artist,
        // title, name);

        p.title = generalFixer(p.title);
        p.artist = generalFixer(p.artist);



        return p;
    }

    private String replaceSquareBrackets(String s) {
        // System.out.println("s = " + s);
        Pattern ptr = Pattern.compile("\\[([^\\]]+)\\]");
        Matcher mchr = ptr.matcher(s);

        while (mchr.find()) {
            for (String end : REMIX_ENDS) {
                if (mchr.group(1).trim().toLowerCase().endsWith(end)) {
                    s = s.substring(0, mchr.start()) + "(" + mchr.group(1)
                            + ")" + s.substring(mchr.end(), s.length());
                    //mchr = ptr.matcher(s);
                    break;
                }
            }

        }

        s = s.replaceAll("\\s+\\)", ")");

        return s;
    }

    private String generalFixer(String name) {

        try {
            name = URLDecoder.decode(name, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        name = StringEscapeUtils.unescapeHtml(name);
        name = name.replace("Â€“", "-");
        name = name.replace("â€“", "-");
        name = name.replaceAll("\u002d+", "-");
        name = name.replaceAll("\u2013+", "-");

        name = name.replace('_', ' ');
        name = name.replace("=", "-");
        name = name.replace("(", " (");
        name = name.replace(",", ", ");
        name = replaceSquareBrackets(name);
        name = name.replaceAll("\\s{2,}", " ");
        // Czy niezbyt ryzykowne: (?)
        name = trimAfterRemix(name);

        if (name.toLowerCase().endsWith(".mp3")) {
            name = name.substring(0, name.length() - 4);
        }

        for (String banned : bannedPrefixes) {
            int index = name.toLowerCase().indexOf(banned.toLowerCase());

            while (index >= 0) {
                name = name.substring(0, index);
                index = name.toLowerCase().indexOf(banned.toLowerCase());
            }
        }

        for (String banned : bannedWords) {
            name = name.replace(banned, " ");
        }

        // wiecej niz jeden myslnik (moze ze spacja pomiedzy):
        name = name.replaceAll("-\\s*-", "-");
        // puste lub prawie puste nawiasy z roznymi znakami (nie literami):
        name = name.replaceAll("\\([\\.\\-\\\\,/_\\s\\(\\)]*\\)", "");
        name = name.replaceAll("\\s{2,}", " ");

        // liczby na poczatku (z . lub z _ lub -)
        name = name.replaceAll("^\\d+[-_\\.]+", "");
        // name = name.replaceAll("\\[.*?\\]", "");

        name = name.replaceAll("/{2,}[^/]*$", "");

        name = name.replaceAll("\\\\{2,}[^\\\\]*$", "");

        if (name.endsWith(".")) {
            name = name.substring(0, name.length() - 1);
        }

        name = name.replaceAll("\\(\\s+", "(");
        name = name.replaceAll("\\s+\\)", ")");

        name = name.trim();
        Pattern ptr = Pattern.compile("(?i)^(.*?)\\s+by\\s+(\\S+)$");
        Matcher mchr = ptr.matcher(name);
        if (mchr.find()) {
            String group = mchr.group(2);
            if (isWebPageAdress(group)) {
                name = mchr.group(1).trim();
            }
        }

        ptr = Pattern.compile("(?i)^(.*?\\))\\s+by\\s+(\\S+)$");
        mchr = ptr.matcher(name);
        if (mchr.find()) {
            name = mchr.group(1).trim();
        }

        name = capitalizeFirstLetters(name);
        name = name.replaceAll("^[\\-\\.]*(.*?)[\\-\\.]*$", "$1").trim();
        // System.out.println("Name = " + name);
        return name;
    }

    private String titleFixer(String title) {

        String dash = " - ";
        int index = title.indexOf(dash);
        if (index < 0) {
            dash = "-";
            index = title.indexOf(dash);
        }

        if (index >= 0) {

            for (String mix : REMIX_ENDS) {
                if (title.toLowerCase().endsWith(mix)) {
                    String part1 = title.substring(0, index).trim();
                    String part2 = title.substring(index + dash.length(),
                            title.length()).trim();
                    title = part1 + " (" + part2 + ")";
                    break;
                }
            }
        }

        return title;
    }

    private String getRating(Tag tag) {

        Iterator iterator = tag.getFields();
        while (iterator.hasNext()) {
            TagField t = ((TagField) iterator.next());

            if (t.getId().equalsIgnoreCase("TXXX")) {
                try {
                    String s = new String(t.getRawContent());
                    String[] parts = s.split("[\\s\0]+");

                    if (parts.length < 3
                            || s.toLowerCase().contains("rating") == false) {
                        return "";
                    }

                    return parts[2].trim();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }

        return "";
    }

    private String yearFixer(String year) {
        year = year.trim();
        Pattern yearPatter = Pattern.compile("^(\\d\\d\\d\\d).*$");
        Matcher mchr = yearPatter.matcher(year);
        if (mchr.find()) {
            year = mchr.group(1);
        } else {
            year = "";
        }
        return year;
    }

    private boolean isWebPageAdress(String s) {
        if (s.contains(" ")) {
            return false;
        }

        s = s.toLowerCase();


        if (s.startsWith("www") || s.startsWith("http")) {
            return true;
        }

        if (s.endsWith(".com")) {
            return true;
        }

        return false;
    }

    private String getRegexpString(String s) {
        s = s.replace("\\", "\\\\");
        s = s.replace("\"", "\\\"");
        s = s.replace("[", "\\[");
        s = s.replace("]", "\\]");
        s = s.replace("*", "\\*");
        s = s.replace("+", "\\+");
        s = s.replace("?", "\\?");
        s = s.replace(".", "\\.");
        s = s.replace("^", "\\^");
        s = s.replace("$", "\\$");
        s = s.replace("&", "\\&");
        s = s.replace("|", "\\|");
        s = s.replace("{", "\\{");
        s = s.replace("}", "\\}");
        s = s.replace(")", "\\)");
        s = s.replace("(", "\\(");
        return s;
    }

    private void fixArtistDuplicate(MyTag p) {
        //String title = titleTF.getText();
        //String artist = artistTF.getText();
        if (p.title.equals("") || p.artist.equals("")) {
            return;
        }
        System.out.printf("%s\t%s\t%s%n", p.artist, getRegexpString(p.artist), "(?i)^" + getRegexpString(p.artist) + "\\s+-\\s+(.*)$");
        Pattern ptr = Pattern.compile("(?i)^" + getRegexpString(p.artist) + "\\s+-\\s+(.*)$");
        Matcher mchr = ptr.matcher(p.title);
        if (mchr.find()) {
            p.title = mchr.group(1);
        }
    }

    public MyTag readTagAlternative(File f) throws IOException, ID3ReadException {
        MyTag myTag = new MyTag();
        myTag.setMp3File(f);

        //  System.out.println("FILE: " + f.getAbsolutePath());
        MusicMetadataSet src_set = new MyID3().read(f);

        if (src_set == null) {
            System.out.println("Plik " + f.getAbsolutePath()
                    + "\nnie posiada tagów ID3v1/ID3v2.");
        } else {

            java.util.List<UnknownUserTextValue> list = src_set.merged.getUnknownUserTextValues();
            for (UnknownUserTextValue mm : list) {
                if (mm.key.toLowerCase().trim().equals("rating")) {
                    myTag.rating = mm.value.trim();
                }
                //  System.out.println("mm = " + mm.key + " = " + mm.value);
            }

            String artist = src_set.merged.getArtist();
            String title = src_set.merged.getSongTitle();
            Number y = src_set.merged.getYear();
            String year = y == null ? "" : y.toString();
            if (artist == null) {
                artist = "";
            }
            if (title == null) {
                title = "";
            }

            String genre = src_set.merged.getGenreName();
            if (genre == null) {
                genre = "";
            }
            Number tr = src_set.merged.getTrackNumberNumeric();
            String track = tr == null ? "" : tr.toString();
            String album = src_set.merged.getAlbum();
            if (album == null) {
                album = "";
            }

            myTag.artist = artist;
            myTag.title = title;
            myTag.year = year;
            myTag.genre = genre;
            myTag.album = album;
            myTag.track = track;

//            System.out.printf(
//                    "artsit = %s; title = %s; year = %s; genre = %s\n",
//                    artist, title, year, genre);
        }
        return myTag;
    }

    public MyTag readTag(File f) throws CannotReadException, IOException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
        //  System.out.println("FILE: " + f.getAbsolutePath());
        AudioFile audioFile = AudioFileIO.read(f);
        Tag tag = audioFile.getTag();


        if (tag == null) {
            System.out.println("Plik " + f.getAbsolutePath()
                    + "\nnie posiada tagów ID3v1/ID3v2.");

            MyTag myTag = new MyTag();
            myTag.setMp3File(f);
            return myTag;
        } else {
            String rating = getRating(tag);

            String artist = tag.getFirst(FieldKey.ARTIST);
            String title = tag.getFirst(FieldKey.TITLE);
            String year = tag.getFirst(FieldKey.YEAR);
            String genre = tag.getFirst(FieldKey.GENRE);
            Artwork art = tag.getFirstArtwork();

            String standardRating = tag.getFirst(FieldKey.RATING);
            String track = tag.getFirst(FieldKey.TRACK);

            String album = tag.getFirst(FieldKey.ALBUM);

            MyTag myTag = new MyTag(artist, title, year, genre);
            myTag.standardRating = standardRating;
            myTag.track = track;
            myTag.album = album;
            myTag.artwork = art;


            myTag.setMp3File(f);
            myTag.setRating(rating);
            return myTag;
        }
    }

    public MyTag getFixedTag(MyTag tag) {
        MyTag resultTag = new MyTag();
        resultTag.setMp3File(tag.getMp3File());

        resultTag.setRating(tag.getRating());


        if (tag.isEmpty()) {
            Pair p = getTitleAndArtistFromName(tag.getMp3File().getName());
            resultTag.setTitle(generalFixer(p.title));
            resultTag.setArtist(generalFixer(p.artist));
        } else {
            resultTag.year = yearFixer(tag.year);

            resultTag.title = generalFixer(tag.title.trim());
            resultTag.artist = generalFixer(tag.artist.trim());

            resultTag.track = tag.track.trim();

            resultTag.standardRating = tag.standardRating.trim();

            resultTag.album = generalFixer(tag.album);

            resultTag.artwork = tag.artwork;


            if (resultTag.artist.equalsIgnoreCase("va")) {
                resultTag.artist = "";
            }

            if (resultTag.artist.equalsIgnoreCase("artist")) {
                resultTag.artist = "";
            }

            if (resultTag.title.equalsIgnoreCase("va")) {
                resultTag.title = "";
            }

            if (resultTag.title.equalsIgnoreCase(resultTag.artist)) {
                resultTag.title = "";
            }

            resultTag.title = titleFixer(resultTag.title);

            resultTag.year = resultTag.year.trim();

            if (resultTag.artist.trim().equals("") && resultTag.title.trim().equals("")) {

                Pair p = getTitleAndArtistFromName(resultTag.mp3File.getName());

                resultTag.artist = p.artist;
                resultTag.title = p.title;


            } else if (resultTag.title.trim().equals("")) {

                if ((resultTag.artist.contains("-") == false && resultTag.getMp3File().getName().contains(
                        "-"))
                        || isWebPageAdress(resultTag.artist)) {

                    Pair p = getTitleAndArtistFromName(resultTag.mp3File.getName());
                    resultTag.artist = p.artist;
                    resultTag.title = p.title;

                } else {
                    Pair p = getTitleAndArtistFromName(resultTag.artist);
                    resultTag.artist = p.artist;
                    resultTag.title = p.title;
                }

            } else if (resultTag.artist.trim().equals("")) {

                if ((resultTag.title.contains("-") == false && resultTag.mp3File.getName().contains(
                        "-"))
                        || isWebPageAdress(resultTag.title)) {
                    Pair p = getTitleAndArtistFromName(resultTag.mp3File.getName());
                    resultTag.artist = p.artist;
                    resultTag.title = p.title;
                } else {
                    Pair p = getTitleAndArtistFromName(resultTag.title);
                    resultTag.artist = p.artist;
                    resultTag.title = p.title;
                }
            }
        }

        fixArtistDuplicate(resultTag);
        resultTag.title = fixOriginalMix(resultTag.title);

        return resultTag;
    }

    private void setRatingTag(Tag tag, String rating) {
        if (rating.trim().equals("")) {
            return;
        }
        tag.deleteField("TXXX");

        ID3v24Frame frame = new ID3v24Frame("TXXX");
        ((FrameBodyTXXX) frame.getBody()).setDescription("RATING");
        ((FrameBodyTXXX) frame.getBody()).setText(rating);

        try {
            tag.setField(frame);
        } catch (FieldDataInvalidException e) {
            e.printStackTrace();
        }
    }

    public void saveTag(File f, MyTag t) throws IOException, CannotReadException,
            TagException, ReadOnlyFileException,
            InvalidAudioFrameException,
            CannotWriteException {
        TagFinder.removeTags(f);
        AudioFile audioFile = AudioFileIO.read(f);

        Tag tag = audioFile.getTagOrCreateAndSetDefault();
        tag.setField(FieldKey.ARTIST, t.artist);
        // title
        tag.setField(FieldKey.TITLE, t.title);
        // genre

        tag.setField(FieldKey.GENRE, t.genre);
        // year
        tag.setField(FieldKey.YEAR, t.year);

        if (t.hasRating()) {
            setRatingTag(tag, t.rating);
        } else if (t.hasStandardRating()) {
            tag.setField(FieldKey.RATING, t.standardRating);
        }


        if (t.track.equals("") == false) {
            tag.setField(FieldKey.TRACK, t.track);
        }

        if (t.album.equals("") == false) {
            tag.setField(FieldKey.ALBUM, t.album);
        }

        if (t.artwork != null) {
            tag.setField(t.artwork);
        }

        AudioFileIO.write(audioFile);
        System.out.println("save ok :) " + f.getAbsolutePath() + "");
    }

    public static String formatFileName(String filename) {
        filename = filename.replace('"', '\'');
        for (char c : PROHIBITED_CHARS) {
            filename = filename.replace(c, ' ');
        }

        filename = filename.replaceAll(
                "^[\\-\\.]*(.*?)[\\-\\.]*(\\s+\\(\\d\\d\\d\\d\\))?$", "$1$2").trim();
        filename = filename.replaceAll("\\s{2,}", " ");
        return filename;
    }

    private String trimAfterRemix(String s) {
        for (String rmx : REMIX_ENDS) {
            String word = " " + rmx + ")";
            int index = s.toLowerCase().indexOf(word);

            if (index >= 0) {
                s = s.substring(0, index + word.length());
            }
        }

        return s;
    }

    private String fixOriginalMix(String s) {
        String orig = " original mix ";
        int index = s.toLowerCase().indexOf(orig);
        if (index >= 0) {
            s = s.substring(0, index) + " (Original Mix) " + s.substring(index + orig.length());
            return s;
        }

        orig = " original remix ";
        index = s.toLowerCase().indexOf(orig);
        if (index >= 0) {
            s = s.substring(0, index) + " (Original Remix) " + s.substring(index + orig.length());
            return s;
        }

        orig = " original mix";
        index = s.toLowerCase().indexOf(orig);
        if (index >= 0 && index + orig.length() == s.length()) {
            s = s.substring(0, index) + " (Original Mix)";
            return s;
        }

        orig = " original remix";
        index = s.toLowerCase().indexOf(orig);
        if (index >= 0 && index + orig.length() == s.length()) {
            s = s.substring(0, index) + " (Original Remix)";
            return s;
        }

        return s;
    }

    public static File getCopyFile(File existingFile) {
        String fileName = existingFile.getAbsolutePath();
        int index = 1;
        File f = null;

        Pattern ptr = Pattern.compile("(?i)^(.*)\\.COPY\\d+\\.mp3$");

        do {
            Matcher mchr = ptr.matcher(fileName);
            if (mchr.find()) {
                fileName = mchr.group(1) + ".COPY" + index + ".mp3";
            } else {
                fileName = fileName.substring(0, fileName.length() - 4) + ".COPY" + index + ".mp3";
            }
            f = new File(fileName);
            index++;
        } while (f.exists() && f.isFile());

        return f;
    }

    private void logMessage(PrintWriter out, String msg) {

        if (out != null) {
            out.println(msg);
        }
    }

    public void logTag(MyTag orig, MyTag resultTag, PrintWriter out) {

        logMessage(out, "\tORIGINAL ARTIST: " + orig.artist);
        logMessage(out, "\tORIGINAL TITLE: " + orig.title);
        logMessage(out, "\tORIGINAL YEAR: " + orig.year);
        logMessage(out, "\tORIGINAL GENRE: " + orig.genre);
        logMessage(out, "\tORIGINAL STANDARD RATING: " + orig.rating);
        logMessage(out, "\tORIGINAL TRACK: " + orig.track);
        logMessage(out, "\tORIGINAL ALBUM: " + orig.album);
        logMessage(out, "\tORIGINAL RATING: " + orig.rating + "\n");

        logMessage(out, "\tRESULT ARTIST: " + resultTag.artist);
        logMessage(out, "\tRESULT TITLE: " + resultTag.title);
        logMessage(out, "\tRESULT YEAR: " + resultTag.year);
        logMessage(out, "\tRESULT GENRE: " + resultTag.genre);
        logMessage(out, "\tRESULT STANDARD RATING: " + resultTag.rating);
        logMessage(out, "\tRESULT TRACK: " + resultTag.track);
        logMessage(out, "\tRESULT ALBUM: " + resultTag.album);
        logMessage(out, "\tRESULT RATING: " + resultTag.rating + "\n");

        logMessage(out, "\tNEW NAME: " + resultTag.mp3File.getName());
    }

    public static File getLogFile(String name) {
        return new File(name
                + String.format(" %1$td-%1$tm-%1$tY, %1$tT", Calendar.getInstance()).replace(":", "-") + ".txt");
    }

    public static void main(String[] args) {
        MusicUtil mu = new MusicUtil();
        String s = "ble - ble (Feat. ble Edit) adfa original mix sdfa";
        System.out.println(s);
        s = mu.fixOriginalMix(s);
        System.out.println(s);
    }
}
