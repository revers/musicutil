package musicutil;

import com.jtechlabs.ui.widget.directorychooser.JDirectoryChooser;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.logging.LogManager;
import java.util.prefs.Preferences;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import musicutil.mp3.MyTag;
import org.apache.commons.lang.StringEscapeUtils;
import pl.ugu.revers.swing.ErrorDialog;
import pl.ugu.revers.swing.ExtendedFormattedTextField;

public class MusicUtilFrame extends JFrame {

    private File bannedPrefixesFile = new File("BANNED.txt");
    private File bannedWordsFile = new File("BANNED_WORDS.txt");
    public static final String TITLE = "MusicUtil";
    public static final String VERSION = "1.5.1 (S)";
    public static final String TITLE_WITH_VERSION = TITLE + " " + VERSION;
    private static final String PATH_PREF = "dir";
    private JButton openJB = new JButton("Otwórz...");
    private JButton refreshJB = new JButton("Odśwież");
    private JButton nextJB = new JButton("Następny");
    private JButton exitJB = new JButton("Wyjście");
    private JButton saveAndNextJB = new JButton("Zapisz/Następny");
    private JButton copyFromOrigJB = new JButton("Kopiuj tag do Edycji");
    private JButton autoJB = new JButton("Automatycznie...");
    private JButton extractFromFilenameJB = new JButton("Ekstraktuj A&T z nazwy pliku");
    private JLabel dirJL = new JLabel("Wybierz katalog...");
    private JLabel artistJL = new JLabel("Artysta: ");
    private JLabel titleJL = new JLabel("Tytuł: ");
    private JLabel yearJL = new JLabel("Rok: ");
    private JLabel befJL = new JLabel("Przed: ");
    private JLabel afJL = new JLabel("Po: ");
    private JLabel ratingJL = new JLabel("Ocena: ");
    private ExtendedFormattedTextField ratingTF = new ExtendedFormattedTextField("");
    private ExtendedFormattedTextField beforeTF = new ExtendedFormattedTextField("");
    private JLabel afterJL = new JLabel("");
    private JCheckBox genreCB = new JCheckBox("Gatunek: ");
    private ExtendedFormattedTextField genreTF = new ExtendedFormattedTextField();
    private JCheckBox fileRenameCB = new JCheckBox("FileNameRenamer");
    private ExtendedFormattedTextField artistTF = new ExtendedFormattedTextField();
    private ExtendedFormattedTextField titleTF = new ExtendedFormattedTextField();
    private ExtendedFormattedTextField yearTF = new ExtendedFormattedTextField();
    private TitledBorder titledBorder = new TitledBorder("Nazwa pliku (0/0): ");
    private JLabel origArtistJL = new JLabel("Artysta: ");
    private JLabel origTitleJL = new JLabel("Tytuł: ");
    private JLabel origYearJL = new JLabel("Rok: ");
    private JLabel origRatingJL = new JLabel("Ocena: ");
    private ExtendedFormattedTextField origRatingTF = new ExtendedFormattedTextField("");
    private JLabel origGenreJL = new JLabel("Gatunek: ");
    private ExtendedFormattedTextField origGenreTF = new ExtendedFormattedTextField();
    private ExtendedFormattedTextField origArtistTF = new ExtendedFormattedTextField();
    private ExtendedFormattedTextField origTitleTF = new ExtendedFormattedTextField();
    private ExtendedFormattedTextField origYearTF = new ExtendedFormattedTextField();
    private ArrayList<File> taggedList;
    private int index = 0;
    private String result;
    private boolean formatName = true;
    private MusicUtil musicUtil;
    private MyTag resultTag;
    public String[] bannedPrefixes;
    public String[] bannedWords;
    private PrintWriter out;
    private MyTag origTag;
    private File currentFile;

    public MusicUtilFrame() {
        initComponents();

        this.genreCB.setMargin(new Insets(0, 0, 0, 0));
        this.genreCB.setBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
        setDefaultCloseOperation(0);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("Nie moge ustawic LookAndFeel");
        }
        SwingUtilities.updateComponentTreeUI(this);

        this.bannedPrefixes = readBannedPrefixes();
        this.bannedWords = readBannedWords();
        this.musicUtil = new MusicUtil(this.bannedPrefixes, this.bannedWords);

        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                MusicUtilFrame.this.closeLog();
                System.exit(0);
            }
        });
    }

    private void closeLog() {
        if (this.out != null) {
            this.out.close();
        }
        this.out = null;
    }

    private String[] readBannedWords() {
        if (!this.bannedWordsFile.exists()) {
            String[] bannedWords = MusicUtil.STANDARD_BANNED_WORDS;
            saveStandardBannedWords();
            return bannedWords;
        }
        String[] bannedWords;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(this.bannedWordsFile), "UTF16"));

            ArrayList bannedList = new ArrayList();
            String s;
            while ((s = in.readLine()) != null) {
                s = s.trim();
                if (s.equals("")) {
                    continue;
                }
                bannedList.add(s.trim());
            }
            in.close();

            bannedWords = (String[]) bannedList.toArray(new String[0]);
        } catch (IOException e) {
            ErrorDialog.showError(e);
            bannedWords = MusicUtil.STANDARD_BANNED_WORDS;
        }

        return bannedWords;
    }

    private void saveStandardBannedWords() {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.bannedWordsFile), "UTF16")));

            for (String banned : MusicUtil.STANDARD_BANNED_WORDS) {
                out.println(banned);
            }

            out.close();
        } catch (IOException e) {
            ErrorDialog.showError(e);
        }
    }

    private String[] readBannedPrefixes() {
        if (!this.bannedPrefixesFile.exists()) {
            String[] bannedPrefixes = MusicUtil.STANDARD_BANNED_PREFIXES;
            saveStandardBannedPrefixes();
            return bannedPrefixes;
        }
        String[] bannedPrefixes;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(this.bannedPrefixesFile), "UTF16"));

            ArrayList bannedList = new ArrayList();
            String s;
            while ((s = in.readLine()) != null) {
                s = s.trim();
                if (s.equals("")) {
                    continue;
                }
                bannedList.add(s.trim());
            }
            in.close();

            bannedPrefixes = (String[]) bannedList.toArray(new String[0]);
        } catch (IOException e) {
            ErrorDialog.showError(e);
            bannedPrefixes = MusicUtil.STANDARD_BANNED_PREFIXES;
        }

        return bannedPrefixes;
    }

    private void saveStandardBannedPrefixes() {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.bannedPrefixesFile), "UTF16")));

            for (String banned : MusicUtil.STANDARD_BANNED_PREFIXES) {
                out.println(banned);
            }

            out.close();
        } catch (IOException e) {
            ErrorDialog.showError(e);
        }
    }

    private JPanel createOrigTagPanel() {
        this.origRatingTF.setEditable(false);
        this.origGenreTF.setEditable(false);
        this.origArtistTF.setEditable(false);
        this.origTitleTF.setEditable(false);
        this.origYearTF.setEditable(false);

        JPanel editPanel = new JPanel();
        editPanel.setBorder(new CompoundBorder(new TitledBorder("Oryginalne: "), new EmptyBorder(0, 5, 0, 5)));

        editPanel.setLayout(new GridBagLayout());
        GridBagConstraints g2 = new GridBagConstraints();

        g2.fill = 0;
        int anchor = 13;
        g2.anchor = anchor;
        g2.weightx = 0.0D;
        g2.insets = new Insets(0, 0, 5, 5);

        editPanel.add(this.origArtistJL, g2);

        g2.gridy = 1;
        editPanel.add(this.origTitleJL, g2);

        g2.gridy = 2;
        editPanel.add(this.origYearJL, g2);

        g2.gridx = 2;

        g2.gridx = 1;
        g2.fill = 2;
        g2.weightx = 1.0D;
        g2.gridwidth = 2;
        editPanel.add(this.origYearTF, g2);

        g2.gridy = 1;

        editPanel.add(this.origTitleTF, g2);

        g2.gridy = 0;
        editPanel.add(this.origArtistTF, g2);

        g2.gridy = 3;
        g2.gridx = 0;
        g2.fill = 0;
        g2.anchor = anchor;

        g2.weightx = 0.0D;
        g2.gridwidth = 1;
        editPanel.add(this.origGenreJL, g2);

        g2.gridx = 1;
        g2.weightx = 1.0D;
        g2.gridwidth = 1;
        g2.fill = 2;
        editPanel.add(this.origGenreTF, g2);

        g2.gridwidth = 1;
        g2.gridx = 2;
        g2.fill = 2;
        g2.weightx = 0.0D;
        JPanel ratingPanel = new JPanel();
        ratingPanel.setLayout(new BoxLayout(ratingPanel, 2));
        ratingPanel.add(Box.createHorizontalStrut(5));
        ratingPanel.add(this.origRatingJL);
        ratingPanel.add(Box.createHorizontalStrut(5));
        ratingPanel.add(this.origRatingTF);

        this.copyFromOrigJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.yearTF.setText(MusicUtilFrame.this.origYearTF.getText().trim());
                MusicUtilFrame.this.genreTF.setText(MusicUtilFrame.this.origGenreTF.getText().trim());
                MusicUtilFrame.this.artistTF.setText(MusicUtilFrame.this.origArtistTF.getText().trim());
                MusicUtilFrame.this.titleTF.setText(MusicUtilFrame.this.origTitleTF.getText().trim());
                MusicUtilFrame.this.genreCB.setSelected(true);
                MusicUtilFrame.this.genreTF.setEnabled(MusicUtilFrame.this.genreCB.isSelected());
            }
        });
        return editPanel;
    }

    private void initComponents() {
        URL url = ClassLoader.getSystemResource("icon.png");
        Image icon = null;
        if (url != null) {
            icon = new ImageIcon(url).getImage();
        } else {
            icon = Toolkit.getDefaultToolkit().getImage("icon.png");
        }

        setIconImage(icon);

        this.fileRenameCB.setSelected(this.formatName);
        this.beforeTF.setEditable(false);
        this.genreCB.setSelected(false);
        setTitle(TITLE_WITH_VERSION);
        setSize(500, 520);
        setMinimumSize(getSize());
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        int x = dim.width / 2 - getWidth() / 2;
        int y = dim.height / 2 - getHeight() / 2;
        setLocation(x, y);

        setDefaultCloseOperation(3);

        JPanel firstPanel = new JPanel();
        JPanel secondPanel = new JPanel();

        firstPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        secondPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

        firstPanel.setLayout(new GridBagLayout());

        JPanel openPanel = new JPanel();
        openPanel.setBorder(new CompoundBorder(new TitledBorder("Katalog źródłowy: "), new EmptyBorder(0, 5, 5, 5)));

        openPanel.setLayout(new GridBagLayout());
        GridBagConstraints g1 = new GridBagConstraints();
        g1.gridx = 1;

        openPanel.add(this.openJB, g1);
        g1.gridx = 0;
        g1.fill = 2;

        g1.anchor = 17;
        g1.weightx = 1.0D;

        openPanel.add(this.dirJL, g1);

        GridBagConstraints g = new GridBagConstraints();

        g.fill = 2;
        g.weightx = 1.0D;
        firstPanel.add(openPanel, g);

        JPanel origPanel = createOrigTagPanel();
        g.gridy = 1;
        firstPanel.add(origPanel, g);

        JPanel editPanel = new JPanel();
        editPanel.setBorder(new CompoundBorder(new TitledBorder("Edycja: "), new EmptyBorder(0, 5, 0, 5)));

        editPanel.setLayout(new GridBagLayout());
        GridBagConstraints g2 = new GridBagConstraints();

        g2.fill = 0;
        int anchor = 13;
        g2.anchor = anchor;
        g2.weightx = 0.0D;
        g2.insets = new Insets(0, 0, 5, 5);

        editPanel.add(this.artistJL, g2);

        g2.gridy = 1;
        editPanel.add(this.titleJL, g2);

        g2.gridy = 2;
        editPanel.add(this.yearJL, g2);

        g2.gridx = 2;

        g2.gridx = 1;
        g2.fill = 2;
        g2.weightx = 1.0D;
        editPanel.add(this.yearTF, g2);

        g2.gridy = 1;
        g2.gridwidth = 2;

        editPanel.add(this.titleTF, g2);

        g2.gridy = 0;
        editPanel.add(this.artistTF, g2);

        g2.gridy = 3;
        g2.gridx = 0;
        g2.fill = 0;
        g2.anchor = anchor;

        g2.weightx = 0.0D;
        g2.gridwidth = 1;
        editPanel.add(this.genreCB, g2);

        g2.gridx = 1;
        g2.weightx = 1.0D;
        g2.gridwidth = 1;
        g2.fill = 2;
        editPanel.add(this.genreTF, g2);

        //g2.fill = 2;
        //g2.weightx = 1.0;
        g2.gridy = 4;
        g2.gridx = 0;
        g2.fill = GridBagConstraints.NONE;
        g2.gridwidth = 2;
        g2.weightx = 0.0D;
        g2.insets = new Insets(2, 5, 5, 5);
        editPanel.add(extractFromFilenameJB, g2);

        JPanel ratingPanel = new JPanel();
        ratingPanel.setLayout(new BoxLayout(ratingPanel, 2));
        ratingPanel.add(Box.createHorizontalStrut(5));
        ratingPanel.add(this.ratingJL);
        ratingPanel.add(Box.createHorizontalStrut(5));
        ratingPanel.add(this.ratingTF);


        g.gridy = 2;
        firstPanel.add(editPanel, g);

        JPanel previewPanel = new JPanel();
        previewPanel.setBorder(new CompoundBorder(this.titledBorder, new EmptyBorder(0, 5, 0, 5)));

        previewPanel.setLayout(new GridBagLayout());
        GridBagConstraints g3 = new GridBagConstraints();
        g3.anchor = 17;
        g3.weightx = 0.0D;

        g3.gridy = 0;
        g3.insets = new Insets(5, 0, 5, 0);

        previewPanel.add(this.befJL, g3);
        g3.gridy = 1;
        previewPanel.add(this.afJL, g3);
        g3.fill = 2;
        g3.anchor = 13;
        g3.weightx = 1.0D;
        g3.gridx = 1;
        previewPanel.add(this.afterJL, g3);
        g3.gridy = 0;
        previewPanel.add(this.beforeTF, g3);

        g.gridy = 3;
        firstPanel.add(previewPanel, g);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, 0));
        buttonsPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
        buttonsPanel.add(this.autoJB);
        buttonsPanel.add(Box.createHorizontalGlue());
        buttonsPanel.add(this.saveAndNextJB);
        buttonsPanel.add(Box.createHorizontalStrut(5));
        buttonsPanel.add(this.nextJB);
        buttonsPanel.add(Box.createHorizontalStrut(5));

        Dimension d = this.refreshJB.getPreferredSize();
        this.nextJB.setPreferredSize(d);
        this.exitJB.setPreferredSize(d);

        g.gridy = 4;
        g.weighty = 1.0D;

        g.anchor = 20;
        firstPanel.add(buttonsPanel, g);

        add(firstPanel, "Center");

        setAllEnabled(false);

        this.genreCB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.genreTF.setEnabled(MusicUtilFrame.this.genreCB.isSelected());
            }
        });
        this.exitJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        this.openJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.openJBAction();
            }
        });
        this.refreshJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.fileRenameCB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.formatName = !MusicUtilFrame.this.formatName;
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.artistTF.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.titleTF.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.yearTF.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.nextJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.nextJBAction();
            }
        });
        this.artistTF.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
            }

            public void insertUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }

            public void removeUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.titleTF.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
            }

            public void insertUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }

            public void removeUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.yearTF.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
            }

            public void insertUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }

            public void removeUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.genreTF.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
            }

            public void insertUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }

            public void removeUpdate(DocumentEvent e) {
                MusicUtilFrame.this.refreshAction();
            }
        });
        this.saveAndNextJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                MusicUtilFrame.this.saveAndNextJBAction();
            }
        });
        this.autoJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Tak", "Nie"};
                int n = JOptionPane.showOptionDialog(MusicUtilFrame.this, "Pełna automatyzacja jest niezalecane ze względu\nna możliwość błędnego uzupełnienia tagów.\n(Jednak wszystkie zmiany sa zapisywane do pliku z logiem,\ntak zeby w razie czego mozna je bylo recznie odtworzyc).\n\nCzy jesteś pewien, że chcesz użyć pełnej automatyzacji?", "Ostrzeżenie!", 0, 2, null, options, options[1]);

                if (n == 1) {
                    return;
                }

                EventQueue.invokeLater(new Runnable() {

                    public void run() {
                        AutomaticDialog dialog = new AutomaticDialog(MusicUtilFrame.this, true);
                        dialog.setVisible(true);
                    }
                });
            }
        });

        this.extractFromFilenameJB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                extractAction();
            }
        });

        addMenu();
    }

    private void addMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("Plik");
        JMenu toolsMenu = new JMenu("Narzędzia");
        JMenu helpMenu = new JMenu("Pomoc");
        menuBar.add(fileMenu);
        menuBar.add(toolsMenu);
        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        JMenuItem exitItem = new JMenuItem("Wyjście");
        fileMenu.add(exitItem);

        JMenuItem helpItem = new JMenuItem("O programie...");
        helpMenu.add(helpItem);

        JMenuItem optionsItem = new JMenuItem("Opcje...");
        toolsMenu.add(optionsItem);

        optionsItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                new OptionsDialog(MusicUtilFrame.this, true).setVisible(true);
            }
        });
        exitItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MusicUtilFrame.this.closeLog();
                System.exit(0);
            }
        });
        helpItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String txt = "Autor:  Kamil Kołaczyński\nKontakt: revers666@o2.pl";

                JOptionPane.showMessageDialog(MusicUtilFrame.this, txt, TITLE_WITH_VERSION, 1);
            }
        });
    }

    private void logMessage(String msg) {
        if (this.out != null) {
            this.out.println(msg);
        }
    }

    private void saveAndNextJBAction() {
        this.saveAndNextJB.setEnabled(false);
        this.nextJB.setEnabled(false);

        new Thread() {

            public synchronized void start() {
                if ((Options.getInstance().isSaveLog()) && (MusicUtilFrame.this.out == null)) {
                    File logFile = MusicUtil.getLogFile("Tryb standardowy");
                    try {
                        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile), "UTF16")));
                    } catch (IOException ex) {
                        ErrorDialog.showError(ex);
                    }
                }

                MusicUtilFrame.this.resultTag.artist = MusicUtilFrame.this.artistTF.getText();
                MusicUtilFrame.this.resultTag.genre = MusicUtilFrame.this.genreTF.getText();
                MusicUtilFrame.this.resultTag.title = MusicUtilFrame.this.titleTF.getText();
                MusicUtilFrame.this.resultTag.year = MusicUtilFrame.this.yearTF.getText();
                try {
                    File f = (File) MusicUtilFrame.this.taggedList.get(MusicUtilFrame.this.index);
                    File renameFile = new File(f.getParent() + "\\" + MusicUtilFrame.this.result);

                    MusicUtilFrame.this.logMessage(f.getAbsolutePath());

                    if (!f.renameTo(renameFile)) {
                        renameFile = MusicUtil.getCopyFile(renameFile);
                        if (!f.renameTo(renameFile)) {
                            throw new IOException("ERROR: nie moge zmienic nazwy pliku z:\n'" + f.getAbsolutePath() + "'\nna:\n'" + renameFile.getAbsolutePath() + "'!!!");
                        }

                    }

                    MusicUtilFrame.this.resultTag.mp3File = renameFile;
                    MusicUtilFrame.this.musicUtil.logTag(MusicUtilFrame.this.origTag, MusicUtilFrame.this.resultTag, MusicUtilFrame.this.out);

                    MusicUtilFrame.this.musicUtil.saveTag(renameFile, MusicUtilFrame.this.resultTag);

                    MusicUtilFrame.this.logMessage("Plik zapisany pomyslnie :)");
                    MusicUtilFrame.this.logMessage("----------------------------------------------------------");
                } catch (Exception e) {
                    MusicUtilFrame.this.logMessage("ERROR :( " + e.getMessage());
                    MusicUtilFrame.this.logMessage("----------------------------------------------------------");
                    JOptionPane.showMessageDialog(MusicUtilFrame.this, e.getMessage(), "ERROR! " + e, 0);

                    e.printStackTrace();
                }

                MusicUtilFrame.this.nextJBAction();

            }
        }.start();
    }

    private void clearTFs() {
        this.artistTF.setText("");
        this.titleTF.setText("");
        this.ratingTF.setText("");
        this.yearTF.setText("");
    }

    private void nextJBAction() {
        this.index += 1;

        if (this.index >= this.taggedList.size()) {
            this.index = (this.taggedList.size() - 1);
            this.saveAndNextJB.setEnabled(false);
            this.extractFromFilenameJB.setEnabled(false);
            closeLog();
            return;
        } else if (this.index == this.taggedList.size() - 1) {
            this.nextJB.setEnabled(false);
        } else {
            MusicUtilFrame.this.nextJB.setEnabled(true);
        }
        
        clearTFs();
        readTag();
        refreshAction();
    }

    private void setAllEnabled(boolean b) {
        this.copyFromOrigJB.setEnabled(b);
        this.origArtistJL.setEnabled(b);
        this.origTitleJL.setEnabled(b);
        this.origYearJL.setEnabled(b);
        this.origGenreJL.setEnabled(b);
        this.origRatingJL.setEnabled(b);

        this.origRatingTF.setEnabled(b);
        this.origGenreTF.setEnabled(b);
        this.origArtistTF.setEnabled(b);
        this.origTitleTF.setEnabled(b);
        this.origYearTF.setEnabled(b);
        this.extractFromFilenameJB.setEnabled(b);

        this.artistJL.setEnabled(b);
        this.titleJL.setEnabled(b);
        this.yearJL.setEnabled(b);
        this.befJL.setEnabled(b);
        this.afJL.setEnabled(b);
        this.beforeTF.setEnabled(b);
        this.afterJL.setEnabled(b);
        this.fileRenameCB.setEnabled(b);
        this.artistTF.setEnabled(b);
        this.titleTF.setEnabled(b);
        this.yearTF.setEnabled(b);
        this.refreshJB.setEnabled(b);
        this.nextJB.setEnabled(b);
        this.saveAndNextJB.setEnabled(b);
        this.genreCB.setEnabled(b);
        if ((b == true) && (this.genreCB.isSelected())) {
            this.genreTF.setEnabled(b);
        } else {
            this.genreTF.setEnabled(false);
        }
        this.ratingJL.setEnabled(b);
        this.ratingTF.setEnabled(b);
    }

    private void openJBAction() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());

        String defaultValue = File.listRoots()[1].getPath();
        String path = prefs.get(PATH_PREF, defaultValue);

        File dir = null;
        if ((dir = JDirectoryChooser.showDialog(this, new File(path))) != null) {
            closeLog();

            clearTFs();
            prefs.put(PATH_PREF, dir.getAbsolutePath());

            this.dirJL.setText(dir.getAbsolutePath());
            int total = 0;
            File[] files = dir.listFiles();
            this.taggedList = new ArrayList();
            for (File f : files) {
                if ((!f.getName().toLowerCase().endsWith(".mp3")) || (f.isDirectory())) {
                    continue;
                }
                total++;

                this.taggedList.add(f);
            }
            if (this.taggedList.size() == 1) {
                this.nextJB.setEnabled(false);
            } else if (this.taggedList.size() == 0) {
                return;
            }

            setAllEnabled(true);

            this.index = 0;
            readTag();
            refreshAction();
        }
    }

    private void extractAction() {
        if (currentFile == null) {
            return;
        }
        MusicUtil.Pair pair = musicUtil.getTitleAndArtistFromName(currentFile.getName());

        if ((pair.artist.equals("")) && (pair.title.equals(""))) {
            this.artistJL.setForeground(Color.red);
            this.titleJL.setForeground(Color.red);
        } else if (pair.artist.equals("")) {
            this.artistJL.setForeground(Color.red);
            this.titleJL.setForeground(Color.black);
        } else if (pair.title.equals("")) {
            this.artistJL.setForeground(Color.black);
            this.titleJL.setForeground(Color.red);
        }

        this.titleTF.setText(pair.title);
        this.artistTF.setText(pair.artist);
    }

    private void readTag() {
        File f = (File) this.taggedList.get(this.index);
        currentFile = f;

        this.artistJL.setForeground(Color.black);
        this.titleJL.setForeground(Color.black);

        this.origRatingTF.setText("");
        this.origGenreTF.setText("");
        this.origArtistTF.setText("");
        this.origTitleTF.setText("");
        this.origYearTF.setText("");
        try {
            try {
                this.origTag = this.musicUtil.readTag(f);
            } catch (Exception e) {
                this.origTag = this.musicUtil.readTagAlternative(f);
            }

            if (this.origTag.isEmpty()) {
                this.artistJL.setForeground(Color.red);
                this.titleJL.setForeground(Color.red);
            } else {
                this.origRatingTF.setText(this.origTag.getRating());
                this.origGenreTF.setText(this.origTag.getGenre());
                this.origArtistTF.setText(this.origTag.getArtist());
                this.origTitleTF.setText(this.origTag.getTitle());
                this.origYearTF.setText(this.origTag.getYear());

                if ((this.origTag.artist.equals("")) && (this.origTag.title.equals(""))) {
                    this.artistJL.setForeground(Color.red);
                    this.titleJL.setForeground(Color.red);
                } else if (this.origTag.artist.equals("")) {
                    this.artistJL.setForeground(Color.red);
                    this.titleJL.setForeground(Color.black);
                } else if (this.origTag.title.equals("")) {
                    this.artistJL.setForeground(Color.black);
                    this.titleJL.setForeground(Color.red);
                }
            }

            this.resultTag = this.musicUtil.getFixedTag(this.origTag);
            this.titleTF.setText(this.resultTag.title);
            this.artistTF.setText(this.resultTag.artist);

            this.ratingTF.setText(this.resultTag.rating);
            this.yearTF.setText(this.resultTag.year);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage() + "\nPomijam plik:\n" + f.getAbsolutePath(), "ERROR!", 0);

            e.printStackTrace();
            nextJBAction();
            return;
        }

        String name = f.getName();
        name = name.substring(0, name.length() - 4);
        this.beforeTF.setText(name);
        String s = "Nazwa pliku (" + (this.index + 1) + "/" + this.taggedList.size() + "): ";

        this.titledBorder.setTitle(s);

        refreshAction();
        repaint();
    }

    private void refreshAction() {
        this.result = (this.artistTF.getText() + " - " + this.titleTF.getText());
        if ((Options.getInstance().isIncludeYearInFilename()) && (!this.yearTF.getText().equals(""))) {
            this.result = (this.result + " (" + this.yearTF.getText() + ")");
        }
        if (this.formatName) {
            this.result = MusicUtil.capitalizeFirstLetters(this.result);
        }
        this.result = MusicUtil.formatFileName(this.result);
        this.result += ".mp3";
        this.afterJL.setText(this.result);

        MusicUtilFrame.this.saveAndNextJB.setEnabled(true);
    }

    public void test() {
        String s = "Rock's Massive - Rock's Massive [porter Robinson Remix    ] and [other mix  ]";
        System.out.println(s);

        System.out.println(s);
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println("ble = " + StringEscapeUtils.unescapeHtml("%20") + URLDecoder.decode("%20", "UTF-8"));

        LogManager.getLogManager().reset();

        EventQueue.invokeLater(new Thread() {

            public void run() {
                MusicUtilFrame m = new MusicUtilFrame();
                m.setVisible(true);
            }
        });
    }
}