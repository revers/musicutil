package musicutil;

import java.util.prefs.Preferences;

/**
 *
 * @author Revers
 */
public class Options {

    private static final String INCLUDE_RATING_PREF = "rat";
    private static final String INCLUDE_TRACK_PREF = "tra";
    private static final String INCLUDE_ALBUM_PREF = "alb";
    private static final String INCLUDE_ARTWORK_PREF = "art";
    private static final String SAVE_LOG_PREF = "log";
    private static final String INCLUDE_YEAR_IN_FILENAME_PREF = "year";
    private boolean includeYearInFilename = true;
    private boolean includeRating = true;
    private boolean includeTrack = false;
    private boolean includeAlbum = false;
    private boolean includeArtwork = false;
    private boolean saveLog = true;
    private Preferences prefs = Preferences.userNodeForPackage(getClass());

    private Options() {
        String includeYearInFilenameStr = prefs.get(INCLUDE_YEAR_IN_FILENAME_PREF,
                Boolean.toString(includeYearInFilename));
        includeYearInFilename = Boolean.parseBoolean(includeYearInFilenameStr);

        String includeArtworkStr = prefs.get(INCLUDE_ARTWORK_PREF,
                Boolean.toString(includeArtwork));
        includeArtwork = Boolean.parseBoolean(includeArtworkStr);

        String includeRatingStr = prefs.get(INCLUDE_RATING_PREF,
                Boolean.toString(includeRating));
        includeRating = Boolean.parseBoolean(includeRatingStr);

        String includeTrackStr = prefs.get(INCLUDE_TRACK_PREF,
                Boolean.toString(includeTrack));
        includeTrack = Boolean.parseBoolean(includeTrackStr);

        String includeAlbumStr = prefs.get(INCLUDE_ALBUM_PREF,
                Boolean.toString(includeAlbum));
        includeAlbum = Boolean.parseBoolean(includeAlbumStr);

        String saveLogStr = prefs.get(SAVE_LOG_PREF,
                Boolean.toString(saveLog));
        saveLog = Boolean.parseBoolean(saveLogStr);
    }

    public static Options getInstance() {
        return OptionsHolder.INSTANCE;
    }

    private static class OptionsHolder {

        private static final Options INSTANCE = new Options();
    }

    public boolean isIncludeYearInFilename() {
        return includeYearInFilename;
    }

    public void setIncludeYearInFilename(boolean includeYearInFilename) {
        this.includeYearInFilename = includeYearInFilename;
        prefs.put(INCLUDE_YEAR_IN_FILENAME_PREF, Boolean.toString(includeYearInFilename));
    }

    public boolean isIncludeAlbum() {
        return includeAlbum;
    }

    public void setIncludeAlbum(boolean includeAlbum) {
        this.includeAlbum = includeAlbum;
        prefs.put(INCLUDE_ALBUM_PREF, Boolean.toString(includeAlbum));
    }

    public boolean isIncludeRating() {
        return includeRating;
    }

    public void setIncludeRating(boolean includeRating) {
        this.includeRating = includeRating;
        prefs.put(INCLUDE_RATING_PREF, Boolean.toString(includeRating));
    }

    public boolean isIncludeTrack() {
        return includeTrack;
    }

    public void setIncludeTrack(boolean includeTrack) {
        this.includeTrack = includeTrack;
        prefs.put(INCLUDE_TRACK_PREF, Boolean.toString(includeTrack));
    }

    public boolean isSaveLog() {
        return saveLog;
    }

    public void setSaveLog(boolean saveLog) {
        this.saveLog = saveLog;
        prefs.put(SAVE_LOG_PREF, Boolean.toString(saveLog));
    }

    public boolean isIncludeArtwork() {
        return includeArtwork;
    }

    public void setIncludeArtwork(boolean includeArtwork) {
        this.includeArtwork = includeArtwork;
        prefs.put(INCLUDE_ARTWORK_PREF, Boolean.toString(includeArtwork));
    }
}
