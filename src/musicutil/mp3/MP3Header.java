package musicutil.mp3;



public class MP3Header implements Comparable<MP3Header> {
	// Special Values:
	private static final int FREE = 0;
	private static final int BAD = -1;
	private static final int RESERVED = Integer.MIN_VALUE;

	public static final int[][][] BITRATES = {
			{
					{ FREE, 32000, 64000, 96000, 128000, 160000, 192000,
							224000, 256000, 288000, 320000, 352000, 384000,
							416000, 448000, BAD }, // V1_L1
					{ FREE, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
							128000, 160000, 192000, 224000, 256000, 320000,
							384000, BAD }, // V1_L2
					{ FREE, 32000, 40000, 48000, 56000, 64000, 80000, 96000,
							112000, 128000, 160000, 192000, 224000, 256000,
							320000, BAD }, // V1_L3
			},
			{
					{ FREE, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
							128000, 144000, 160000, 176000, 192000, 224000,
							256000, BAD }, // V2_L1
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD }, // V2_L2
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD } // V2_L3
			},
			{
					{ FREE, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
							128000, 144000, 160000, 176000, 192000, 224000,
							256000, BAD }, // V2.5_L1
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD }, // V2.5_L2
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD } // V2.5_L3
			} };

	public static final int[][] SAMPLERATES = {
			{ 44100, 48000, 32000, RESERVED }, // MPEG1
			{ 22050, 24000, 16000, RESERVED }, // MPEG2
			{ 11025, 12000, 8000, RESERVED } // MPEG2.5
	};

	private static final int[][] SAMPLES_PER_FRAME = { //
	{ 384, 1152, 1152 }, // MPEG-1
			{ 384, 1152, 576 }, // MPEG-2
			{ 384, 1152, 576 }, // MPEG-2.5
	};

	private static void calculateFrameTimes() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				// for(int k = 0; k < 2; k++) {
				for (int l = 0; l < 3; l++) {
					System.out.print(" " + SAMPLERATES[i][j]
							/ SAMPLES_PER_FRAME[i][l]);
				}
				// }
				System.out.println();

			}
		}
	}

	// 11 MSBs set (11111111111000000000000000000000):
	private static final int SYNC_BITS = -2097152;

	// MPEG Versions:
	public static final int MPEG1 = 0;
	public static final int MPEG2 = 1;
	public static final int MPEG2_5 = 2;

	// Layer Version:
	public static final int LAYER_I = 0;
	public static final int LAYER_II = 1;
	public static final int LAYER_III = 2;

	// Channel Modes:
	public static final int STEREO = 0;
	public static final int JOINT_STEREO = 1;
	public static final int DUAL_CHANNEL = 2;
	public static final int MONO = 3;

	// private boolean isInitialFrame;
	private int headerBits;

	private int mpegLayer;
	private int mpegVersion;
	private int bitRate;
	private int sampleRate;
	private int frameLength;
	private int channelMode;
	private int modeExtension;
	private int emphasis;
	private int samplesPerFrame;

	private boolean hascrc;
	private boolean framePadding;
	private boolean priv;
	private boolean copyright;
	private boolean original;
	private int streamPos = -1;

	public MP3Header(int header) throws IncorrectMP3HeaderException {
		this.headerBits = header;

		// Sprawdz czy 11 pierwszych MSB jest ustawionych:
		if ((header & SYNC_BITS) != SYNC_BITS)
			throw new IncorrectMP3HeaderException(
					"Podany naglowek \""
							+ asBinaryString(header)
							+ "\" nie zaczyna sie 11-stoma bitami ustawionymi (sync frame).");

		mpegVersion = calculateMPEGVersion();
		mpegLayer = calculateMPEGLayer();
		hascrc = calculateHasCRC();
		bitRate = calculateBitRate();
		sampleRate = calulateSampleRate();
		framePadding = calculatePadingBit();
		priv = calculatePrivate();
		channelMode = calculateChannelMode();
		modeExtension = calculateModeExtension();
		copyright = calculateCopyright();
		original = calculateOriginal();
		emphasis = calculateEmphasis();

		if (channelMode != 1 && modeExtension != 0)
			throw new IncorrectMP3HeaderException(
					"Mode Extension = "
							+ modeExtension
							+ " a powinien miec wartosc 0 dla Channel Mode roznego od 1 (channelMode = "
							+ channelMode + ")");

		samplesPerFrame = calculateSamplesPerFrame();
		frameLength = calculateFrameLength();
	}

	private final int calculateFrameLength() {
		int padding = 0;
		if (mpegLayer == LAYER_I) {
			if (framePadding)
				padding = 4;
			return (12 * bitRate / sampleRate + padding) * 4;
		} else {
			if (framePadding)
				padding = 1;
			return 144 * bitRate / sampleRate + padding;
		}
	}

	private final int calculateSamplesPerFrame() {
		return SAMPLES_PER_FRAME[mpegVersion][mpegLayer];
	}

	private final int calculateEmphasis() {
		return headerBits & 3;
	}

	private final boolean calculateOriginal() {
		return ((headerBits >> 2) & 1) == 1 ? true : false;
	}

	private final boolean calculateCopyright() {
		return ((headerBits >> 3) & 1) == 1 ? true : false;
	}

	private final int calculateModeExtension() {
		return (headerBits >> 4) & 3;
	}

	private final int calculateChannelMode() {
		return (headerBits >> 6) & 3;
	}

	private final boolean calculatePrivate() {
		return ((headerBits >> 8) & 1) == 1 ? true : false;
	}

	private final boolean calculatePadingBit() {
		return ((headerBits >> 9) & 1) == 1 ? true : false;
	}

	private final int calulateSampleRate() throws IncorrectMP3HeaderException {
		int sampleRateIndex = (headerBits >> 10) & 3;
		int sr = SAMPLERATES[mpegVersion][sampleRateIndex];
		if (sr == RESERVED)
			throw new IncorrectMP3HeaderException(
					"Sample Rate ma bledna wartosc: RESERVED");
		return sr;
	}

	private final int calculateBitRate() throws IncorrectMP3HeaderException {
		/*
		 * if (h_vbr == true){ return ((int) ((h_vbr_bytes 8) / (ms_per_frame()
		 * h_vbr_frames)))1000; } else return bitrates[h_version][h_layer -
		 * 1][h_bitrate_index]; }
		 */
		int bitRateIndex = (headerBits >> 12) & 15;
		int br = BITRATES[mpegVersion][mpegLayer][bitRateIndex];
		if (br == BAD)
			throw new IncorrectMP3HeaderException(
					"Bit Rate ma bledna wartosc: BAD");
		return br;
	}

	private final boolean calculateHasCRC() {
		return ((headerBits >> 16) & 1) == 0 ? true : false;
	}

	private final int calculateMPEGVersion() throws IncorrectMP3HeaderException {
		int version = (headerBits >> 19) & 3;

		switch (version) {
		case 0:
			return MPEG2_5;
		case 1:
			throw new IncorrectMP3HeaderException(
					"Nie poprawna wersja: RESERVED");
		case 2:
			return MPEG2;
		case 3:
			return MPEG1;

		}

		// Nigdy nie nastapi. Wszystkie 4 opcje rozwazone.
		return -1;
	}

	private final int calculateMPEGLayer() throws IncorrectMP3HeaderException {
		int layer = (headerBits >> 17) & 3;

		switch (layer) {
		case 0:
			throw new IncorrectMP3HeaderException(
					"Nie poprawna warstwa: RESERVED");
		case 1:
			return LAYER_III;
		case 2:
			return LAYER_II;
		case 3:
			return LAYER_I;
		}

		// Nigdy nie nastapi. Wszystkie 4 przypadki rozpatrzone.
		return -1;
	}

	private double[] h_vbr_time_per_frame = { -1, 384, 1152, 1152 };
	public static final int[][] frequencies = { { 22050, 24000, 16000, 1 },
			{ 44100, 48000, 32000, 1 }, { 11025, 12000, 8000, 1 } };

	public float getMsPerFrame(boolean isVBR) {
		if (isVBR == true) {
			double tpf = h_vbr_time_per_frame[mpegLayer] / sampleRate;
			if ((mpegVersion == MPEG2) || (mpegVersion == MPEG2_5))
				tpf /= 2;
			return ((float) (tpf * 1000));
		} else {
			float ms_per_frame_array[][] = { { 8.707483f, 8.0f, 12.0f },
					{ 26.12245f, 24.0f, 36.0f }, { 26.12245f, 24.0f, 36.0f } };
			int sampleRateIndex = (headerBits >> 10) & 3;
			return (ms_per_frame_array[mpegLayer - 1][sampleRateIndex]);
		}
	}

	public int getHeaderBits() {
		return headerBits;
	}

	public int getSamplesPerFrame() {
		return samplesPerFrame;
	}

	public int getMpegLayer() {
		return mpegLayer;
	}

	public int getMpegVersion() {
		return mpegVersion;
	}

	public int getBitRate() {
		return bitRate;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public int getFrameLength() {
		return frameLength;
	}

	public int getChannelMode() {
		return channelMode;
	}

	public int getModeExtension() {
		return modeExtension;
	}

	public int getEmphasis() {
		return emphasis;
	}

	public boolean hasCRC() {
		return hascrc;
	}

	public boolean isFramePadded() {
		return framePadding;
	}

	public boolean isPrivate() {
		return priv;
	}

	public boolean isCopyright() {
		return copyright;
	}

	public boolean isOriginal() {
		return original;
	}
	
	

	public int getStreamPos() {
		return streamPos;
	}

	public void setStreamPos(int streamPos) {
		this.streamPos = streamPos;
	}

	// wykorzystane w printHeaderLayout(). Mam nadzieje ze
	// dzieki final kompilator z-inline'uje ta metode...
	private static final String insertSpace(String s, int pos) {
		return s.substring(0, pos) + " " + s.substring(pos);
	}

	public static void printHeaderLayout(int frameHeader) {
		String s = asBinaryString(frameHeader);

		s = insertSpace(s, 11);
		s = insertSpace(s, 14);
		s = insertSpace(s, 17);
		s = insertSpace(s, 19);
		s = insertSpace(s, 24);
		s = insertSpace(s, 27);
		s = insertSpace(s, 29);
		s = insertSpace(s, 31);
		s = insertSpace(s, 34);
		s = insertSpace(s, 37);
		s = insertSpace(s, 39);
		s = insertSpace(s, 41);

		System.out.println("AAAAAAAAAAA BB CC D EEEE FF G H II JJ K L MM");
		System.out.println(s);

	}

	public static String asBinaryString(int i) {
		StringBuffer buf = new StringBuffer();
		int mask = 1;
		for (int j = 31; j >= 0; j--) {
			if (((mask << j) & i) != 0)
				buf.append("1");
			else
				buf.append("0");
		}

		return buf.toString();

	}

	public void printValues() {
		System.out.println("getMPEGVersion() = " + getMpegVersion());
		System.out.println("getMPEGLayer() = " + getMpegLayer());
		System.out.println("hasCRC() = " + hasCRC());
		System.out.println("getBitrate() = " + getBitRate());
		System.out.println("getSamplerate() = " + getSampleRate());
		System.out.println("isFramePadded() = " + isFramePadded());
		System.out.println("getFrameLength() = " + getFrameLength());
		System.out.println("isPrivate() = " + isPrivate());
		System.out.println("getChannelMode() = " + getChannelMode());
		System.out.println("getModeExtension() = " + getModeExtension());
		System.out.println("isCopyright() = " + isCopyright());
		System.out.println("isOriginal() = " + isOriginal());
		System.out.println("getEmphasis() = " + getEmphasis());
		System.out.println("getSamplesPerFrame() = " + getSamplesPerFrame());
	}

	public static void main(String[] args) {
		printHeaderLayout(-290048);

		try {
			MP3Header m = new MP3Header(-290048);
			m.printValues();
			/*
			 * self.size = (144self.bitrate) / self.samplerate + self.padding
			 * 
			 * 
			 * if self.bitrate: self.duration = self.size8.0/self.bitrate else:
			 * self.duration = 0.0
			 */
			System.out.println("DURATION = "
					+ (m.getFrameLength() * 8 / (m.getBitRate() / 1000)));
			// System.out.println(m.getBitRate());
		} catch (IncorrectMP3HeaderException e) {
			e.printStackTrace();
		}
		System.out.println(asBinaryString(0xFFFFFF3F));
		calculateFrameTimes();

	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof MP3Header))
			return false;

		MP3Header m = (MP3Header) o;
		if (getMpegVersion() != m.getMpegVersion())
			return false;

		// layer change never possible
		if (getMpegLayer() != m.getMpegLayer())
			return false;

		// sampling rate change never possible
		if (getSampleRate() != m.getSampleRate())
			return false;

		// from mono to stereo never possible
		if ((getChannelMode() == 3) != (m.getChannelMode() == 3))
			return false;

		if (getEmphasis() != m.getEmphasis())
			return false;

		return true;
	}

	@Override
	public int compareTo(MP3Header m) {
		if (m == null)
			return -1;

		// version change never possible
		if (getMpegVersion() != m.getMpegVersion())
			return -1;

		// layer change never possible
		if (getMpegLayer() != m.getMpegLayer())
			return -1;

		// sampling rate change never possible
		if (getSampleRate() != m.getSampleRate())
			return -1;

		// from mono to stereo never possible
		if ((getChannelMode() == 3) != (m.getChannelMode() == 3))
			return -1;

		if (getEmphasis() != m.getEmphasis())
			return -1;

		return 0;
	}

}
