package musicutil.mp3;

import java.io.File;
import org.jaudiotagger.tag.datatype.Artwork;

/**
 *
 * @author Revers
 */
public class MyTag {

    public File mp3File;
    public String artist = "";
    public String title = "";
    public String year = "";
    public String genre = "";
    public String rating = "";
    public String standardRating = "";
    public String track = "";
    public String album = "";
    public Artwork artwork;

    public MyTag(String artist, String title, String year, String genre) {
        this.artist = artist;
        this.title = title;
        this.year = year;
        if (genre != null) {
            this.genre = genre;
        }
        this.rating = "";

    }

    public MyTag(String artist, String title, String year) {
        this(artist, title, year, "");
    }

    public MyTag(String artist, String title) {
        this(artist, title, "", "");
    }

    public MyTag() {
        this("", "", "", "");
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public File getMp3File() {
        return mp3File;
    }

    public void setMp3File(File mp3File) {
        this.mp3File = mp3File;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public boolean hasRating() {
        return rating.equals("") == false;
    }

    public boolean hasStandardRating() {
        return standardRating.equals("") == false;
    }

    public boolean isEmpty() {
        return artist.equals("") && title.equals("") && year.equals("") && genre.equals("") && rating.equals("");
    }

    @Override
    public String toString() {
        return String.format(
                "artsit = %s; title = %s; year = %s; genre = %s",
                artist, title, year, genre);
    }
}
