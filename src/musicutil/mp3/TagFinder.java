package musicutil.mp3;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.farng.mp3.AbstractMP3Tag;
import org.farng.mp3.MP3File;
import org.farng.mp3.TagException;

import cn.cooper.id3.*;

public class TagFinder {

	public static boolean hasTag(File f) {

		cn.cooper.id3.MP3File mp3;
		try {
			mp3 = new cn.cooper.id3.MP3File(f, "r", true);
			if (mp3.hasID3v1Tag || mp3.hasID3v2Tag) {
				mp3.close();
				return true;
			}
			mp3.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public static TagInfo getTag(File f) {
		if (f == null)
			return null;
		// TODO: zamienic na: cn.cooper.id3.MP3File
		MP3File mp3 = null;
		try {
			mp3 = new MP3File(f);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TagException e) {
			e.printStackTrace();
		}
		if (mp3 == null)
			return null;

		AbstractMP3Tag tag = null;
		if (mp3.getID3v2Tag() != null)
			tag = mp3.getID3v2Tag();
		else if (mp3.getID3v1Tag() != null)
			tag = mp3.getID3v1Tag();

		if (tag == null)
			return null;

		String artist = tag.getLeadArtist();
		String title = tag.getSongTitle();
		String year = tag.getYearReleased();

		if (artist.startsWith("��")) {
			artist = changeEnc(artist);
			title = changeEnc(title);
		}
		if (year.startsWith("��"))
			year = changeEnc(year);

		artist = artist.trim();
		title = title.trim();
		year = year.trim();
		try {
			int y = Integer.parseInt(year);
			if (y < 1900)
				year = "";
		} catch (NumberFormatException e) {
			year = "";
		}

		return new TagInfo(artist, title, year);
	}

	private static String dealDashes(String s) {
		if (!s.contains("-"))
			return s;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != '-')
				continue;

			if ((i - 1) >= 0 && (i + 1) < s.length()
					&& Character.isDigit(s.charAt(i - 1))
					&& Character.isDigit(s.charAt(i + 1)))
				continue;

			return s.substring(0, i) + " - "
					+ dealDashes(s.substring(i + 1, s.length()));
		}
		return s;
	}

	public static String formatText(String s) {
		s = s.replace('_', ' ');
		s = dealDashes(s);
		// s = s.replace("-", " - ");
		s = s.replace("=", "-");
		s = s.replace("(", " (");
		s = s.replace("Vs.", "vs");
		s = s.replace("Feat.", "feat.");
		s = s.replaceAll("\\s{2,}", " ");
		s = s.trim();
		s = capitalizeFirstLetters(s);

		return s;

	}

	private static final String[] EXCEPTIONS = { "vs", "vs.", "feat", "feat." };

	private static String capitalizeFirstLetters(String s) {
		Pattern p = Pattern.compile("\\b[\\w&&[^\\d]]+[\\S]*");
		Matcher m = p.matcher(s);
		outer: while (m.find()) {

			String gr = m.group().trim();

			char first = gr.charAt(0);
			if (Character.isUpperCase(first))
				continue;

			for (String exc : EXCEPTIONS) {
				if (gr.equals(exc))
					continue outer;
			}

			gr = gr.substring(0, 1).toUpperCase()
					+ gr.substring(1).toLowerCase();
			s = s.substring(0, m.start()) + gr + s.substring(m.end());
		}
		return s;
	}

	public static void removeTags(File inFile) throws IOException {
		boolean hasID3v1 = false;

		cn.cooper.id3.MP3File mp3 = new cn.cooper.id3.MP3File(inFile, "r", true);
		if (mp3.hasID3v1Tag)
			hasID3v1 = true;
		mp3.close();

		int size = (int) inFile.length();
		// System.out.print(inFile.getName() + " ");
		// System.out.print("sz = " + size);
		if (hasID3v1)
			size -= 128;

		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				inFile));
		String path = inFile.getCanonicalPath();
		File temp = new File(path + ".temp");
		BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(temp));
		int toSkip = MP3Utils.findFirstFramePos(inFile);
		size -= toSkip;
		// System.out.println("; toSkip = " + toSkip);
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[1024];
		int readBytes = 0;
		int totalBytes = 0;
		while ((readBytes = in.read(buff)) != -1 && totalBytes <= size) {
			totalBytes += readBytes;
			if (totalBytes > size)
				readBytes -= totalBytes - size;
			out.write(buff, 0, readBytes);

		}

		in.close();
		out.close();

		if (!inFile.delete())
			throw new IOException("Nie moge zamienic oryginalnego pliku!");

		temp.renameTo(inFile);
	}

	// private static void renameMP3File(File f) {
	// MP3File mp3 = null;
	// try {
	// mp3 = new MP3File(f);
	// } catch (IOException e) {
	// e.printStackTrace();
	// } catch (TagException e) {
	// e.printStackTrace();
	// }
	// if (mp3 == null)
	// return;
	//
	// AbstractMP3Tag tag = null;
	// if (mp3.getID3v2Tag() != null)
	// tag = mp3.getID3v2Tag();
	// else if (mp3.getID3v1Tag() != null)
	// tag = mp3.getID3v1Tag();
	//
	// if (tag == null)
	// return;
	//
	// String artist = tag.getLeadArtist();
	// String title = tag.getSongTitle();
	// String year = tag.getYearReleased();
	//
	// if (artist.startsWith("��")) {
	// artist = changeEnc(artist);
	// title = changeEnc(title);
	// }
	// if (year.startsWith("��"))
	// year = changeEnc(year);
	//
	// // year = "(" + year + ")";
	//
	// System.out.printf("%s - %s %s\n", artist, title, year);
	// String name = f.getParent() + "\\" + artist + " - " + title;
	// if (!year.equals(""))
	// name += " (" + year + ")";
	//
	// name += ".mp3";
	//
	// f.renameTo(new File(name));
	//
	// // if(artist.contains(""))
	// // String s = new String(artist.getBytes(),Charset.forName("UTF-16"));
	// // System.out.println(s);
	//
	// }

	private static final String changeEnc(String s) {
		s = s.substring(1);
		StringBuffer buff = new StringBuffer();
		for (int i = 1; i < s.length(); i += 2)
			buff.append(s.charAt(i));
		// s = new String(s.getBytes(), Charset.forName("UTF-16"));
		// if(s.endsWith("?"))
		// s = s.substring(0, s.length() - 1);
		return buff.toString();
	}

	public static void main(String[] args) {
		String s = "[Cuted from Sander Van Doorn-Identity 2008-11-26]-Track-#1.mp3-";
		System.out.println(s);
		System.out.printf("\"%s\"\n", formatText(s));
	}

}
