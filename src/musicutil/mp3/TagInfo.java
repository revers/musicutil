package musicutil.mp3;


public class TagInfo {
	public String artist;
	public String title;
	public String year;
	public TagInfo(String artist, String title, String year) {
		this.artist = artist;
		this.title = title;
		this.year = year;
	}
	
}
